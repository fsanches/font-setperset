# Setperset

A 7x7 font created in the "Fonts.txt" workshop at the Free Culture Forum 2014,
facilitated by Manufactura Independente. It was designed and built in 4 hours.

Read more about the workshop at the [Type:Bits site](https://typebits.gitlab.io).

![Font preview](https://gitlab.com/typebits/font-setperset/-/jobs/artifacts/master/raw/Setperset-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-setperset/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-setperset/-/jobs/artifacts/master/raw/Setperset-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-setperset/-/jobs/artifacts/master/raw/Setperset-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-setperset/-/jobs/artifacts/master/raw/Setperset-Regular.sfd?job=build-font)

## Authors

* Nelson Dieguez, [@nelsondieguez](https://twitter.com/nelsondieguez)
* Oriol Gayán
* Vanessa Pacheco
* Irene Serrano

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
